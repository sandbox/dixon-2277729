<?php

namespace Drupal\uuid\Tests;

use Drupal\simpletest\WebTestBase;

abstract class UuidIndexTestBase extends WebTestBase {

  public static $modules = array('entity_test', 'uuid');

  /**
   * @var \Drupal\uuid\Entity\UuidIndex;
   */
  protected $uuidIndex;

  protected function setUp() {
    parent::setUp();
    $this->uuidIndex = \Drupal::service('entity.uuid_index');
  }
}
